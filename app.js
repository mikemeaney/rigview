//Rigley Rig-Side view server
//Written by Mike Meaney (2016)
//San Diego, California USA

//---- Ye olde required Node.JS requistes ----

//Express for routing from the RigDriver
var express = require('express');
var app = express();  

//Server for well, being a friggin' server
var server = require('http').createServer(app);  
var io = require('socket.io')(server);

//--- What bytes through yonder NoSQL DB breaks? 
//It is the east and FireBase.io is the west! ----
var Firebase = require('firebase');
var fbData = new Firebase('https://rigley2.firebaseio.com/rigdata')

//--- Telleth thyne app the good path of thou bower ---
app.use(express.static(__dirname + '/bower_components'));  
app.use('/bower_components',  express.static(__dirname + '/bower_components'));

app.use(express.static(__dirname + '/css')); 
//app.use('css',  express.static(__dirname + '/css'));

//-- The one ond only route for our application---
app.get('/', function(req, res,next) {  
    res.sendFile(__dirname + '/index.html');
});

//The one and only global varible, the PID because,
//well that's the easiset way of doing this 
var pid = '';

//Route for the Data coming from the rig driver to go to Rigley
app.get('/data', function(req,res){
	res.header('Access-Control-Allow-Origin', '*')

	console.log("--Req @ /data--");

	//var testData = req.query;
	var testData = {
		'RigID' : req.query.rig,
		'PID' : pid,
		'Data': {
			"durration": req.query.durration,
			"timeIn" : req.query.inTime,
			"timeOut" : req.query.outTime
		}
	};
    console.log(testData);

    //alrighty then, let's be ballsy
    //and post to Firebase
    fbData.push(testData);

    //Send some kind of data back to the client
    //other wise that's, well, just rude.
	res.send(testData);
})

//The Socket handler for the browser UI
io.on('connection', function(client) {  


    console.log('Client connected...');

    //The socket for acknowledging the connection from a browser
    client.on('join', function(data) {
        console.log(data);
        client.emit('messages', 'Hello from server');
    });

 	//The socket to handle the PID
 	client.on('pid', function(data){
 		pid = data
 		console.log("The PID: " + pid);
 	});

    //Route for the Status coming from the rig driver.
    //This sends the result out over a socket
	app.get('/status', function(req,res){
		res.header('Access-Control-Allow-Origin', '*');
		var state = req.query.state;
		var rig = req.query.rig;
		client.emit('status', state);
		client.emit('rig', rig);

		//Now makes some decisions about what to do
		//based on status

		//Reset the PID to be blank after each submission from the rig
		if(state == "Done"){
			pid = '';
		}

		
		console.log("--Req @ /status--");
		console.log(state);
		res.send(state);
	})

});


console.log("RigView listening at http://localhost:3000")
server.listen(3000);  
